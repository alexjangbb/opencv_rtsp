// Ini.h: interface for the CIni class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INI_H__53531F70_484D_408E_B0E8_D2309CF6E809__INCLUDED_)
#define AFX_INI_H__53531F70_484D_408E_B0E8_D2309CF6E809__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include <sstream>
#include <vector>
using namespace std;


namespace iglo{

	inline string CString2string(CString cstr)
	{
		CT2CA pszConvertedAnsiString(cstr);
		std::string str(pszConvertedAnsiString);
		return str;
	};

	inline CString string2CString(string str)
	{
		CString cstr(str.c_str());
		return cstr;
	};



	inline CString GetFileNameFromPath(CString strPathName)
	{
		strPathName.Replace(L"\\", L"/");
		int nResult;
		nResult = strPathName.ReverseFind(TCHAR('/'));
		if (nResult != -1)
		{
			CString strfileName = strPathName.Right(strPathName.GetLength() - nResult - 1);
			return strfileName;
		}
		return strPathName;
	};

	inline string GetFileNameFromPathstring(string strPathName){ return CString2string(GetFileNameFromPath(string2CString(strPathName))); };

	inline void fileLog(CString strdir, CString strlog)
	{
		FILE *fTemp;
		if ((fTemp = _wfopen(strdir, L"at")) != NULL)
		{
			fwprintf(fTemp, L"%s\n", strlog);
			fclose(fTemp);
		}
	};

	inline void filestreamLog(string sfdir, string strlog)
	{
		ofstream outputfile;
		outputfile.open(sfdir, std::ofstream::out | std::ofstream::app);
		outputfile << strlog << "\n";
	};

	inline void MoveFolder(CString pszFrom, CString pszTo)
	{
		SHFILEOPSTRUCT fos = { 0 };

		fos.wFunc = FO_MOVE;
		fos.pFrom = (pszFrom);
		fos.pTo = (pszTo);

		fos.fFlags = FOF_SILENT | FOF_NOCONFIRMATION | FOF_NOERRORUI;
		::SHFileOperation(&fos);
	};

	inline void CopyFolder(LPCTSTR pszFrom, LPCTSTR pszTo)
	{
		SHFILEOPSTRUCT fos = { 0 };

		fos.wFunc = FO_COPY;
		fos.pFrom = pszFrom;
		fos.pTo = pszTo;

		::SHFileOperation(&fos);
	};
	
	inline void DeleteFolder(LPCTSTR pszFolder)
	{
		SHFILEOPSTRUCT fos = { 0 };

		fos.wFunc = FO_DELETE;
		fos.pFrom = pszFolder;

		::SHFileOperation(&fos);
	};
}

using namespace iglo;

class CIni  
{
public:
	CIni();
	virtual ~CIni();

	CString m_strIniPathFileName;
	DWORD m_lRetValue;
	vector<string> m_lines;
	vector<vector<string>> m_varrtokens;

	CString GetStrValue(CString strDefault, CString strKey,CString strSection);
	void SetStrValue(CString strValue, CString strKey, CString strSection);

	int GetIntValue(int nDefault, CString strKey,CString strSection);
	void SetIntValue(int nValue, CString strKey, CString strSection);

	int initGetIntValue(int nDefault, CString strKey, CString strSection);
	CString initGetStrValue(CString strDefault, CString strKey, CString strSection);

	void parseTxtfile(CString strFilePath);

	std::vector<std::string> &str_split(const std::string &s, char delim, std::vector<std::string> &elems);
	std::vector<std::string> str_split(const std::string &s, char delim);
	std::vector<std::string> get_lines(const char * _path, const char comment_start, std::vector<std::string> &lines);
	std::vector<std::string> get_lines(const char * _path, const char comment_start);
	vector<CString> string2CStringVec(vector<string>& vecstring);	
	
};

#endif // !defined(AFX_INI_H__53531F70_484D_408E_B0E8_D2309CF6E809__INCLUDED_)
