// Ini.cpp: implementation of the CIni class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Ini.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CIni::CIni()
{
	m_strIniPathFileName = L"Parameter1.ini";
	//m_strIniPathFileName = "c:\\HUNS\\HUNSRoadCondition.ini";


//	CIni Ini;	
//
//	Ini.m_strIniPathFileName = m_strIniPathFileName;
//
//	CString str;
//	str = Ini.GetStrValue("Danta", "m_nNow");
//	m_nNow = atoi(str);
//
//	str.Format("%d", m_nNow);
//	Ini.SetStrValue(str , "Danta", "m_nNow");



}

CIni::~CIni()
{

}


CString CIni::GetStrValue(CString strDefault, CString strKey,CString strSection )
{
	WCHAR ac_Result[255];

	// Get the info from the .ini file	
	m_lRetValue = GetPrivateProfileString(strSection,strKey,
		strDefault ,ac_Result, 255, m_strIniPathFileName);	

	CString strResult(ac_Result);
	return strResult;
}

void CIni::SetStrValue(CString strValue, CString strKey, CString strSection)
{
	WritePrivateProfileString (strSection, strKey, 
                             strValue, m_strIniPathFileName);

	
}

int CIni::GetIntValue(int nDefault, CString strKey,CString strSection)
{
	CString strDefault = _T("");
	CString strReturn = _T("");
	int nReturn = 0;

	strDefault.Format(_T("%d"), nDefault);
	strReturn = GetStrValue ( strDefault,  strKey, strSection );
    nReturn = _wtoi(strReturn);

	return nReturn;

}
void CIni::SetIntValue(int nValue, CString strKey, CString strSection)
{
	
	CString strValue = _T("");
	strValue.Format(_T("%d"), nValue);
	SetStrValue (strValue,  strKey, strSection );	
	
}

int CIni::initGetIntValue(int nDefault, CString strKey, CString strSection)
{
	int getvalue = GetIntValue(nDefault, strKey, strSection);
	if (getvalue == nDefault)
	{
		SetIntValue(getvalue, strKey, strSection);
	}

	return getvalue;
}

CString CIni::initGetStrValue(CString strDefault, CString strKey, CString strSection)
{

	CString getvalue = GetStrValue(strDefault, strKey, strSection);
	if (getvalue == strDefault)
	{
		SetStrValue(getvalue, strKey, strSection);
	}

	return getvalue;
}


std::vector<std::string> & CIni::str_split(	const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}
std::vector<std::string> CIni::str_split(const std::string &s, char delim) 
{
	std::vector<std::string> elems;
	str_split(s, delim, elems);
	return elems;
}


std::vector<std::string> CIni::get_lines(const char * _path, const char comment_start, std::vector<std::string> &lines)
{
	const int MAX_CHARS_PER_LINE = 10240;
	lines.clear();
	// create a file-reading object
	std::ifstream fin;
	fin.open(_path); // open a file
	if (!fin.good()) {
		std::cerr << "Cannot find file <" << _path << ">";
		return lines; // exit if file not found
	}
	while (!fin.eof())
	{
		char buf[MAX_CHARS_PER_LINE];
		fin.getline(buf, MAX_CHARS_PER_LINE);
		for (char *p = buf; *p; ++p)
			if (*p == comment_start) {
				*p = '\0';
				for (char *q = p - 1; q >= buf; --q) {
					if (*q == ' ' || *q == '\t') *q = '\0'; // trim right
					else break;
				}
				break;
			}
		if (strlen(buf) > 0) { // skip empty line
			lines.push_back(buf);
		}
	}
	return lines;
}
std::vector<std::string> CIni::get_lines(const char * _path, const char comment_start)
{
	std::vector<std::string> lines;
	get_lines(_path, comment_start, lines);
	return lines;
}

void CIni::parseTxtfile(CString strFilePath)
{
	m_lines.clear();
	m_varrtokens.clear();

	string sFilePath = CString2string(strFilePath);
	m_lines = get_lines(sFilePath.c_str(), '#');
	
	for (vector<string>::iterator it_line = m_lines.begin(); it_line != m_lines.end(); ++it_line)
	{
		//cout << "line<" << *it_line << ">" << endl;
		vector<string> tokens = str_split(*it_line, ';');
		for (vector<string>::iterator it_token = tokens.begin();
			it_token != tokens.end(); ++it_token) {
			//cout << "\tToken<" << *it_token << ">" << endl;
		}
		m_varrtokens.push_back(tokens);
	}
}

vector<CString> CIni::string2CStringVec(vector<string>& vecstring)
{
	vector<CString> vecCStr;

	for (int i = 0; i < vecstring.size(); i++)
	{
		vecCStr.push_back(string2CString(vecstring[i]));
	}
	return vecCStr;
}