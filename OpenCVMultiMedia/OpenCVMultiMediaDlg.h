
// OpenCVMultiMediaDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"

#include "Ini.h"
#include "OpenDefs.h"
#include "CvvImage.h"

// COpenCVMultiMediaDlg dialog
class COpenCVMultiMediaDlg : public CDialogEx
{
// Construction
public:
	COpenCVMultiMediaDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_OPENCVMULTIMEDIA_DIALOG };

	enum VISION_EVENTLIST
	{
		VISION_DTIME = 0,
		
	};

	CvvImage m_vvImg;
	Mat m_img;

	BOOL m_bInit;
	CRect m_rtDlgClient;
	CRect m_rtImgView;
	CRect m_rtList;
	CString m_strListCtrlColNames;
	
	CString	m_strPathexe;
	vector <CString> m_vecDropfilePath;
	unsigned int m_curIdxDropFile;
	CString m_strDropfilePath;


	void SetProgramExePath();
	CString GetProgramExePath();
	void Init_ListCtrl();
	void Savelog(CString strlog);
	CString getSavelogFilePath();	
	inline CString getSaveLogPath(){ return L"D:/_log/"; };

	void DrawResultImg();
	CWinThread* m_pThreadDemo;
	void ThreadDemo();
	void ThreadDemoBegin();
	void ThreadDemoStop();

	Mat getDefaultimg();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CListCtrl m_list;
//	CStatic m_stcImgView;
	afx_msg void OnDestroy();
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	CStatic m_stcImgView;
	afx_msg void OnBnClickedButtonConnect();
};
