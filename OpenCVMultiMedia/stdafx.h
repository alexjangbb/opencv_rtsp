
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently,
// but are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN            // Exclude rarely-used stuff from Windows headers
#endif

#include "targetver.h"

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS      // some CString constructors will be explicit

// turns off MFC's hiding of some common and often safely ignored warning messages
#define _AFX_ALL_WARNINGS

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions


#include <afxdisp.h>        // MFC Automation classes



#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>           // MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>             // MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <afxcontrolbars.h>     // MFC support for ribbons and control bars


#ifdef _UNICODE
#if defined _M_IX86
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_X64
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif


//#define USE_CUDA   // NVIDIA 6.5 version
#define USE_OPENCV_WORLD

#ifdef _WIN64

//#pragma comment(lib, "../OpenCV/x64/lib/ippicvmt.lib")  // support intel cpu

#if defined(USE_CUDA)
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudaarithm300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudafilters300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudawarping300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudaimgproc300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudafeatures2d300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudaoptflow300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudabgsegm300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudastereo300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudalegacy300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudaobjdetect300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudacodec300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_cudev300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/cuda.lib")
#pragma comment(lib, "../OpenCV/x64/lib/cufft.lib")
#pragma comment(lib, "../OpenCV/x64/lib/cudart.lib")
#pragma comment(lib, "../OpenCV/x64/lib/nppc.lib")
#pragma comment(lib, "../OpenCV/x64/lib/nppi.lib")
#pragma comment(lib, "../OpenCV/x64/lib/npps.lib")
#endif  //(USE_CUDA)

#if defined(USE_OPENCV_WORLD)

#ifdef _DEBUG

#pragma comment(lib, "../OpenCV/x64/lib/opencv_world440d.lib")
//#pragma comment(lib, "../OpenCV/x64/lib/opencv_tracking300d.lib")
//#pragma comment(lib, "../OpenCV/x64/lib/opencv_dnn300d.lib")
#else

#pragma comment(lib, "../OpenCV/x64/lib/opencv_world440.lib")
//#pragma comment(lib, "../OpenCV/x64/lib/opencv_tracking300.lib")
//#pragma comment(lib, "../OpenCV/x64/lib/opencv_dnn300.lib")
#endif

#else //(USE_OPENCV_WORLD)

#if defined(_DEBUG)

#pragma comment(lib, "../OpenCV/x64/lib/opencv_calib3d300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_core300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_flann300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_features2d300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_video300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_videoio300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_videostab300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_highgui300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_imgproc300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_imgcodecs300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_ml300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_objdetect300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_photo300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_stitching300d.lib")

#pragma comment(lib, "../OpenCV/x64/lib/opencv_hal300d.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_tracking300d.lib")

#else //(_DEBUG)

#pragma comment(lib, "../OpenCV/x64/lib/opencv_calib3d300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_core300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_flann300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_features2d300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_video300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_videoio300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_videostab300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_highgui300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_imgproc300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_imgcodecs300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_ml300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_objdetect300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_photo300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_stitching300.lib")
#pragma comment(lib, "../OpenCV/x64/lib/opencv_hal300.lib")

#pragma comment(lib, "../OpenCV/x64/lib/opencv_tracking300.lib")

#endif//(_DEBUG)

#endif  //(USE_OPENCV_WORLD)


#else //_WIN64
#pragma comment(lib, "../OpenCV/x86/lib/ippicvmt.lib")  // support intel cpu

#pragma comment(lib, "../OpenCV/x86/lib/opencv_world300.lib")
#pragma comment(lib, "../OpenCV/x86/lib/opencv_tracking300.lib")
#endif //_WIN64


