#ifndef _OPEN_DEFS_H_
#define _OPEN_DEFS_H_

//#include "OpenDefs.h"


#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>
#include <opencv2/core/utility.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/objdetect.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/ml.hpp>


#if defined(USE_CUDA)
#include "opencv2/cudaobjdetect.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
using namespace cv::cuda;
#endif


//#include <opencv2/highgui/highgui_c.h>

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
//#define _USE_MATH_DEFINES
//#include <math.h>
//CV_PI

using namespace cv;
using namespace cv::ml;
using namespace cv::dnn;
using namespace std;

//cv::Mat source = cv::Mat::zeros(10, 1, CV_32F), dst;
//cv::sort(source, dst, CV_SORT_EVERY_COLUMN | CV_SORT_DESCENDING);



struct stPTZ
{
	Point pos;
	Rect rect;
	uint speed;
};

inline CRect rect2CRect(Rect rt)
{
	CRect rect(CPoint(rt.x, rt.y), CSize(rt.width, rt.height));
	return rect;
};

inline Rect CRect2rect(CRect rt)
{
	Rect rect(rt.left, rt.top, rt.Width(), rt.Height());
	return rect;
};

inline Point centerPoint(Rect rt)
{	
	return Point((rt.br() + rt.tl())*0.5);
};

inline Point2d centerPoint(Rect2d rt)
{
	return Point2d((rt.br() + rt.tl())*0.5);
};

inline double ptdistance(Point2f pt1, Point2f pt2)
{
	return sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
};

inline double ptdistanceRectNormalCenter(Point2f pt, Rect rt)
{
	CRect rect = rect2CRect(rt);

	double dx = rect.CenterPoint().x - pt.x;
	double dy = rect.CenterPoint().y - pt.y;
	dx = 2*dx / rect.Width();
	dy = 2*dy / rect.Height();
	double dist = sqrt(dx*dx + dy*dy);
		
	return dist;
};

inline Point2f pt2InRect(Point2f pt, Rect rt)
{
	if (pt.x < rt.x)
		pt.x = rt.x;
	if (pt.y < rt.y)
		pt.y = rt.y;

	if (pt.x > rt.x + rt.width - 1)
		pt.x = rt.x + rt.width - 1;
	if (pt.y > rt.y + rt.height - 1)
		pt.y = rt.y + rt.height - 1;

	return pt;
};


#endif // _OPEN_DEFS_H_
