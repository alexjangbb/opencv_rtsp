#pragma once
#ifndef CVVIMAGE_CLASS_DEF
#define CVVIMAGE_CLASS_DEF

#include "OpenDefs.h"

class  CvvImage
{
public:
	CvvImage();
	virtual ~CvvImage();


	virtual void  Destroy(void);
		

	virtual void  DrawToHDC( HDC hDCDst, RECT* pDstRect );

	Mat  m_img;

protected:
	
};
typedef CvvImage CImage1;
#endif