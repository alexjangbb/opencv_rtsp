
// OpenCVMultiMediaDlg.cpp : implementation file
//

#include "stdafx.h"
#include "OpenCVMultiMedia.h"
#include "OpenCVMultiMediaDlg.h"
#include "afxdialogex.h"


#include <map>
#include <vector>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif
 

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};
 
CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// COpenCVMultiMediaDlg dialog



COpenCVMultiMediaDlg::COpenCVMultiMediaDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(COpenCVMultiMediaDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_bInit = FALSE;
}

void COpenCVMultiMediaDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);

	DDX_Control(pDX, IDC_STATIC_IMG, m_stcImgView);
}

BEGIN_MESSAGE_MAP(COpenCVMultiMediaDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DESTROY()
	ON_WM_DROPFILES()
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON_CONNECT, &COpenCVMultiMediaDlg::OnBnClickedButtonConnect)
END_MESSAGE_MAP()


// COpenCVMultiMediaDlg message handlers

BOOL COpenCVMultiMediaDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	SetProgramExePath();	
	m_strListCtrlColNames = L"";
	Init_ListCtrl();
	srand((unsigned int)time(NULL));

	GetClientRect(&m_rtDlgClient);
	m_stcImgView.GetWindowRect(&m_rtImgView);
	ScreenToClient(&m_rtImgView);	
		
	m_list.GetWindowRect(&m_rtList);
	ScreenToClient(&m_rtList);	

	//getDefaultimg();

	m_bInit = TRUE;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

Mat COpenCVMultiMediaDlg::getDefaultimg()
{
	String strimg = "/img/default.bmp";
	m_img = imread(CString2string(m_strPathexe) + strimg, IMREAD_ANYDEPTH);

	if (m_img.data)
	{
		m_vvImg.m_img = m_img.clone();
	}

	return m_img;
}

void COpenCVMultiMediaDlg::SetProgramExePath()
{
	TCHAR	szPgmDir[MAX_PATH] = { 0 };
	::GetModuleFileName(NULL, szPgmDir, _MAX_PATH);
	m_strPathexe = szPgmDir;
	m_strPathexe = m_strPathexe.Left(m_strPathexe.ReverseFind(L'\\'));
}
CString COpenCVMultiMediaDlg::GetProgramExePath()
{
	return m_strPathexe;
}

void COpenCVMultiMediaDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void COpenCVMultiMediaDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		DrawResultImg();
		CDialogEx::OnPaint();
	}
}

void COpenCVMultiMediaDlg::DrawResultImg()
{
	//if (!m_vvImg) return;

	
	CWnd* pWnd = GetDlgItem(IDC_STATIC_IMG);
	CClientDC dc(pWnd);
	RECT rect;
	pWnd->GetClientRect(&rect);
	m_vvImg.DrawToHDC(dc.m_hDC, &rect);
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR COpenCVMultiMediaDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void COpenCVMultiMediaDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	ThreadDemoStop();

	m_bInit = FALSE;
	
}


void COpenCVMultiMediaDlg::OnDropFiles(HDROP hDropInfo)
{
	m_vecDropfilePath.clear();

	DWORD dwDropNum;
	
	wchar_t acFullName[256];
	dwDropNum = ::DragQueryFile(hDropInfo, 0xffffffff, NULL, 0);

	for (int i = 0; i < dwDropNum; i++)
	{
		DragQueryFile(hDropInfo, 0, acFullName, 256);
		m_strDropfilePath.Format(L"%s", acFullName);
		m_vecDropfilePath.push_back(m_strDropfilePath);
	}


	// Demo

	if (m_vecDropfilePath.size()>1)
	{
		for (m_curIdxDropFile = 0; m_curIdxDropFile < dwDropNum; m_curIdxDropFile++)
		{
			ThreadDemo();
		}
	}
	else
		ThreadDemoBegin();
	
	
	CDialogEx::OnDropFiles(hDropInfo);
}

void thread_demo(COpenCVMultiMediaDlg* ptr)
{
	ptr->ThreadDemo();
}

void COpenCVMultiMediaDlg::ThreadDemoBegin()
{
	if (m_pThreadDemo)	ThreadDemoStop();

	m_pThreadDemo = AfxBeginThread((AFX_THREADPROC)thread_demo, this);
}

void COpenCVMultiMediaDlg::ThreadDemoStop()
{

	//if (m_pbackgroundmodel)	m_pbackgroundmodel->m_bRun = FALSE;

	DWORD status;
	if (m_pThreadDemo)
		while (GetExitCodeThread(m_pThreadDemo->m_hThread, &status))
		{
			MSG  msg;

			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
				Sleep(10);
		}

	m_pThreadDemo = NULL;

}


void COpenCVMultiMediaDlg::ThreadDemo()
{

	//OpenCVInit(TRUE);
	//Init_backgroundmodel();
	//m_pbackgroundmodel->m_bRun = TRUE;

	//switch (m_Tab_Recognition.m_stCfg.algorithmtype)
	//{

	//case ALGORITHM_TAMPER_DEFOCUS:
	//case ALGORITHM_TAMPER_OCCLUSION:
	//case ALGORITHM_TAMPER_DISPLACEMENT:
	//	m_pbackgroundmodel->m_TamperDetection.mainDemo(m_Tab_Recognition.m_stCfg);
	//	break;

	//case ALGORITHM_ACTION_RECOGNITION:
	//	m_pbackgroundmodel->mainActioinRecognition();
	//	break;

	//default:
	//	m_pbackgroundmodel->mainDemo(m_Tab_Recognition.m_stCfg, m_strDropfilePath);
	//	break;
	//}
	//m_pbackgroundmodel->m_bRun = FALSE;


	//Free_backgroundmodel();
	//OpenCVInit(FALSE);

	OnOK();
}

void COpenCVMultiMediaDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	if (!m_bInit || cx <= 0)
		return;

	CRect rc(0, 0, cx, cy);
		

	CRect rt(&m_rtImgView);	
	rt.right += (cx - m_rtDlgClient.Width());
	rt.bottom += (cy - m_rtDlgClient.Height());

	m_stcImgView.MoveWindow(&rt);
	

	CRect rtlist(&m_rtList);
	rtlist.right += (cx - m_rtDlgClient.Width());
	rtlist.bottom += (cy - m_rtDlgClient.Height());
	rtlist.top += (cy - m_rtDlgClient.Height());

	m_list.MoveWindow(&rtlist);
}


void COpenCVMultiMediaDlg::OnTimer(UINT_PTR nIDEvent)
{
	

	CDialogEx::OnTimer(nIDEvent);
}



void COpenCVMultiMediaDlg::Init_ListCtrl()
{
	m_list.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES | LVS_EX_FLATSB);
	//m_list.SetImageList(&m_Image_lst, LVSIL_SMALL);

	CString strlog = L"";

	LV_COLUMN lvcolumn;
	lvcolumn.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
	lvcolumn.fmt = LVCFMT_CENTER;
		

	m_strListCtrlColNames = strlog;

	CreateDirectory(getSaveLogPath(), NULL);
	Savelog(strlog);
}

void COpenCVMultiMediaDlg::Savelog(CString strlog)
{

	CString strfilePathname = getSavelogFilePath();

	fileLog(strfilePathname, strlog);

}

CString COpenCVMultiMediaDlg::getSavelogFilePath()
{
	/*CString strlogRoot = getSaveLogPath();

	SYSTEMTIME cur_time;
	GetLocalTime(&cur_time);
		
	CString strTime;
	strTime.Format(L"%04d_%02d%02d_%02d%02d_%02d.%03d", cur_time.wYear, cur_time.wMonth, cur_time.wDay, cur_time.wHour, cur_time.wMinute, cur_time.wSecond, cur_time.wMilliseconds);
		
	CString strFilename = GetFileNameFromPath(m_strDropfilePath);
	strFilename = strFilename.Left(strFilename.GetLength() - 4);
	CString strfilePathname = strlogRoot + L"/vt" + strTime.Left(10) + L"_" + strFilename + L".txt";

	return strfilePathname;*/
	return 0;
}


void COpenCVMultiMediaDlg::OnBnClickedButtonConnect()
{
	Mat img_color;


	const std::string videoStreamAddress = "rtsp://admin:qwe1010@192.168.0.222:554/live2.sdp";
	//const std::string videoStreamAddress = "rtsp://admin:qwe1010@192.168.0.222:554/live4.sdp /tcp";
	//const std::string videoStreamAddress = "rtsp://admin:qwe1010@192.168.0.222:554/live2.sdp";



	//비디오 캡쳐 초기화
	//VideoCapture cap(videoStreamAddress, cv::CAP_FFMPEG);
	VideoCapture cap(videoStreamAddress);
	if (!cap.isOpened()) {
		cerr << "에러 - 카메라를 열 수 없습니다.\n";
		return ;
	}


	while (1)
	{
		// 카메라로부터 캡쳐한 영상을 frame에 저장합니다.
		cap.grab();
		/*cap.read(img_color);
		if (img_color.empty()) {
			cerr << "빈 영상이 캡쳐되었습니다.\n";
			break;
		}*/

		if (cap.read(img_color)) { 
			imshow("Color", img_color);
		}

		

		// ESC 키를 입력하면 루프가 종료됩니다. 

		if (waitKey(25) >= 0)
			break;
	}

}
