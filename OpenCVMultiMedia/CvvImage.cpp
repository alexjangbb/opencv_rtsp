#pragma once
#include "stdafx.h"
#include "CvvImage.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void  FillBitmapInfo( BITMAPINFO* bmi, int width, int height, int bpp, int origin )
{
	assert( bmi && width >= 0 && height >= 0 && (bpp == 8 || bpp == 24 || bpp == 32));

	BITMAPINFOHEADER* bmih = &(bmi->bmiHeader);

	memset( bmih, 0, sizeof(*bmih));
	bmih->biSize = sizeof(BITMAPINFOHEADER);
	bmih->biWidth = width;
	bmih->biHeight = origin ? abs(height) : -abs(height);
	bmih->biPlanes = 1;
	bmih->biBitCount = (unsigned short)bpp;
	bmih->biCompression = BI_RGB;
	if( bpp == 8 )
	{
		RGBQUAD* palette = bmi->bmiColors;
		int i;
		for( i = 0; i < 256; i++ )
		{
			palette[i].rgbBlue = palette[i].rgbGreen = palette[i].rgbRed = (BYTE)i;
			palette[i].rgbReserved = 0;
		}
	}
}
CvvImage::CvvImage()
{
	m_img = 0;
}
void CvvImage::Destroy()
{

}
CvvImage::~CvvImage()
{
	Destroy();
}



void  CvvImage::DrawToHDC( HDC hDCDst, RECT* pDstRect ) 
{
	//if( pDstRect && m_img && m_img->depth == IPL_DEPTH_8U && m_img->imageData )
	{
		uchar buffer[sizeof(BITMAPINFOHEADER) + 1024];
		BITMAPINFO* bmi = (BITMAPINFO*)buffer;
		Rect dst = Rect(pDstRect->left, pDstRect->top, pDstRect->right - pDstRect->left, pDstRect->bottom - pDstRect->top);
		int bmp_w = m_img.cols, bmp_h = m_img.rows;
			

		{
			SetStretchBltMode(
				hDCDst,           // handle to device context
				COLORONCOLOR );
		}
		FillBitmapInfo( bmi, bmp_w, bmp_h, (m_img.depth() & 255)*m_img.channels(), 1 );
		::StretchDIBits(
			hDCDst,
			dst.x, dst.y, dst.width, dst.height,
			0, 0, bmp_w, bmp_h,
			m_img.data, bmi, DIB_RGB_COLORS, SRCCOPY );
	}
}




